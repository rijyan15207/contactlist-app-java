
package Project;

import java.util.ArrayList;

public class ContactList 
{
    private ArrayList<Contact> conArr;
    
    public ContactList()
    {
        conArr = new ArrayList<>();
    }
    


    public void createContact(Contact e)
    {
        conArr.add(e);
    }
    


    public int contactSize()
    {
        return conArr.size();
    }
    


    public void removeContact(int index)
    {
        conArr.remove(index);
    }
    


    public Contact searchContact(String name)
    {
        for(Contact e : conArr)
        {
            if(e.getName().equalsIgnoreCase(name))
                return e;
        }
        return null;
    }
    
    
    @Override
    public String toString() {
        return "ContactList{" + " conArr =\n" + conArr + '}';
    }
    
    
}










