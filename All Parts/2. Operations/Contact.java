
package Project;

public class Contact 
{
    // All properties
    private String name;
    private String phone_no;
    private String email;
    
    
    public Contact(String name, String phone_no)
    {
        this.name = name;
        this.phone_no = phone_no;
        this.email = null;
    }
    
    public Contact(String name, String phone_no, String email)
    {
        this.name = name;
        this.phone_no = phone_no;
        this.email = email;
    }
    
    

    
    // Setter & Getter

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    

    
    // toString
    @Override
    public String toString() 
    {
        return "Contact{" + " name = " + name + 
                ", phone_no = " + phone_no + 
                ", email = " + email + "} "; 
    }
    
}










