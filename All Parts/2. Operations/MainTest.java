
package Project;

public class MainTest 
{
    // Contact List APP
    
    // Create New Contact
    // Can View List
    // Search Contact
    // Delete Contact
    
    // Field: Name, Phone no, Email
    // Contact Array
    // Operation: insert, search & Delete
    
    public static void main(String[] args) 
    {
        Contact con1 = new Contact("Rijwan", "01996813160", "rijyan15207@gmail.com");
        Contact con2 = new Contact("Arif", "01796811100");
        
        
        // Storing contact

        ContactList list = new ContactList();
        list.createContact(con1);
        list.createContact(con2);
        



	// Printing contacts

        System.out.println(list);




	// Checking size of contact

        System.out.println("There are " + list.contactSize() + " contacts\n\n");
        
        
        

        // Remove contact

        list.removeContact(1);
        
        

        
        // Search Contact

        Contact data = list.searchContact("riJwAn");
        if(data != null)
            System.out.println(data);
        else
            System.out.println("Not Found...!");
    }
}















