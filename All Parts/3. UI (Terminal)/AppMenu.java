
package Project;

import java.util.ArrayList;
import java.util.Scanner;


public class AppMenu 
{
    /*
    * Menu
    * 1. Create New Contact
    * 2. View All Contacts
    * 3. Search A Contact
    * 4. Delete A Contact
    * 5. Exit
    */
    
    
    public static void main(String[] args) 
    {
        ContactList list = new ContactList();
        // object created of "ContactList" class
        
        int menuItemNumber = 0;
        do 
        { 
            menuView();
            
            Scanner sc1 = new Scanner(System.in);
            menuItemNumber = sc1.nextInt();
            
            switch (menuItemNumber) 
            { 
                case 1:
                    Contact rcv = createContact();
                    list.createContact(rcv);
                    System.out.println("Contact Successfully Created...\n\n");
                    break;
                case 2:
                    viewContacts(list.getContacts());
                    break;
                case 3:
                    searchList(list);
                    break;
                case 4:
                    removeContact(list);
                    break;
                case 5:
                    System.exit(0);
                default:
                    System.out.println("Please, Select a number amoung 1 to 5...");
                    break;
            }
        } 
        while(menuItemNumber != 5);
    }
    
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    
    
    
    private static void menuView()
    {
        System.out.println("   1.  Create New Contact");
        System.out.println("   2.  View All Contacts");
        System.out.println("   3.  Search A Contact");
        System.out.println("   4.  Delete A Contact");
        System.out.println("   5.  Exit");
        System.out.println("------------------------\n");
        
        System.out.print("Enter Your Choice: ");
    } 
    
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    
    
    private static Contact createContact()
    {
        // Contact data type has taken...
        // because it has to be added to ArrayList 
        
        System.out.println("\nFill the below form correctly...\n");
        Scanner sc2 = new Scanner(System.in);
        
        System.out.print("Enter Name: ");
        String name = sc2.nextLine();
        
        System.out.print("Enter Mobile Number: ");
        String phone_no = sc2.nextLine();
        
        System.out.print("Enter Email (Press 'n' if not available): ");
        String email = sc2.nextLine();
        
        
        Contact con;
        // object created of "Contact" class
        
        if(email.equalsIgnoreCase("n"))
        {
            con = new Contact(name, phone_no);
        }
        else
        {
            con = new Contact(name, phone_no, email);
        }
        return con;
    }
    
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    
    
    private static void viewContacts(ArrayList<Contact> conArr)
    { 
        System.out.println("All Contacts...");
        System.out.println("------------------------");
        
        int i = 0;
        System.out.println("No.      Name   \t\tPhone No   \tEmail");
        for (Contact e : conArr) 
        { 
            String email = e.getEmail();
            System.out.printf("%-5d%-20s  %-15s   %-50s\n", i++, e.getName(), e.getPhone_no(), 
                             (email == null) ? "Not Available" : email);
        }
        System.out.println("\n");
    } 
    
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    
    
    private static void searchList(ContactList list)
    {
        System.out.print("Please, Enter a name to search: ");
        
        Scanner sc3 = new Scanner(System.in);
        String name = sc3.nextLine();
        
        Contact data = list.searchContact(name);
        
        if(data != null) 
        {
            System.out.println("No.      Name   \t\tPhone No   \tEmail");
            
            String email = data.getEmail();
            System.out.printf("%-20s  %-15s   %-50s\n", data.getName(), data.getPhone_no(), 
                             (email == null) ? "Not Available" : email);
        } 
        else
            System.out.println("Contact Not Found...!");
    } 
    
    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    
    
    private static void removeContact(ContactList list)
    {
        viewContacts(list.getContacts());
        
        System.out.print("Enter the indicating contact number that you want to delete: ");
        
        Scanner sc4 = new Scanner(System.in);
        int index = sc4.nextInt();
        
        list.removeContact(index);
        System.out.println("\nContact Successfully Deleted...\n");
    }
} 








